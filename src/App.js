import React from 'react';
import './App.css';
// import DashboardComponent from './Components/DashboardComponent/DashboardComponent';
import DashboardList from './Containers/DashboardList/DashboardList';
import DetailsContainer from './Containers/DetailsContainer/DetailsContainer.js';

import { 
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header"></header>
        <Switch>
          <Route exact path="/">
            <DashboardList/>
          </Route>
        
        
          <Route path="/character/:id" component={ DetailsContainer }></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
