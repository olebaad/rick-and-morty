import React from 'react';
import { ListGroup, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './DashboardCard.css'

const DashboardCard = ( {currentCharacter} ) => {

  return (
    <Card className="character-card m-2" style={{ width: "22rem" }}>
      <Card.Header className="character-header">
        <Card.Img variant="top" src={currentCharacter.image} alt="Character image"/>
        <Card.ImgOverlay className="character-img-overlay"> 
          {currentCharacter.name}
        </Card.ImgOverlay>
      </Card.Header>
      <Card.Body className="character-body">
        <ListGroup className="character-list-group">
          <ListGroup.Item className="character-list-group-item">
            <b>Status:</b> <span className="info-right-flush">{currentCharacter.status}</span>
          </ListGroup.Item>
          <ListGroup.Item className="character-list-group-item">
            <b>Species:</b> <span className="info-right-flush">{currentCharacter.species}</span>
          </ListGroup.Item>
          <ListGroup.Item className="character-list-group-item">
            <b>Gender:</b> <span className="info-right-flush">{currentCharacter.gender}</span>
          </ListGroup.Item>
          <ListGroup.Item className="character-list-group-item">
            <b>Origin:</b> <span className="info-right-flush">{currentCharacter.origin.name}</span>
            </ListGroup.Item>
          <ListGroup.Item className="character-list-group-item">
            <b>Last location:</b> <span className="info-right-flush">{currentCharacter.location.name}</span>
          </ListGroup.Item>
        </ListGroup>
      </Card.Body> 
      <Link to={ "/character/" + currentCharacter.id }>  
        <Card.Footer className="card-footer">
          <h3>View Profile</h3>
        </Card.Footer>   
      </Link>
    </Card>
  );
}
export default DashboardCard;
