import React from 'react';
import { ListGroup, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './DetailsCard.css'

const DetailsCard = ( {currentCharacter, currentPlanet} ) => {
  return (
    <div>
      <Card className="detail-card">
        <Card.Header className="detail-header">
          <span className="header-element">
            <Card.Img id="profile-image" variant="left" src={currentCharacter.image} alt="Character"/>
          </span>
          <span id="header-text" className="header-element" >
            <h2>{currentCharacter.name}</h2>
            <b>Status:</b> <span id="annoying-fing-span">{currentCharacter.status}</span>
          </span>
          <span id="back-button">
            <Link to="/">
              <Button variant="secondary">Go Back</Button>
            </Link>
          </span>
        </Card.Header>
        <Card.Body className="detail-body">
          <h4 className="detail-headers">Profile</h4>
          <ListGroup className="detail-list-group">
            <ListGroup.Item className="detail-text">
              <b>Gender:</b> <span className="info-right-flush">{currentCharacter.gender}</span>
            </ListGroup.Item>
            <ListGroup.Item className="detail-text">
              <b>Species:</b> <span className="info-right-flush">{currentCharacter.species}</span>
            </ListGroup.Item>
            <ListGroup.Item className="detail-text">
              <b>Origin:</b> <span className="info-right-flush">{currentCharacter.origin.name}</span>
            </ListGroup.Item>
          </ListGroup>

          <h4 className="detail-headers">Location</h4>

          <ListGroup>
            <ListGroup.Item className="detail-text">
              <b>Current Location:</b> <span className="info-right-flush">{currentPlanet.name}</span>
            </ListGroup.Item>
            <ListGroup.Item className="detail-text">
              <b>Type:</b> <span className="info-right-flush">{currentPlanet.type}</span>
            </ListGroup.Item>
            <ListGroup.Item className="detail-text">
              <b>Dimension:</b> <span className="info-right-flush">{currentPlanet.dimension}</span>
            </ListGroup.Item>
          </ListGroup>
        </Card.Body>      
      </Card>
    </div>
  );
}
export default DetailsCard;
