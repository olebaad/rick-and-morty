import React from 'react';
import DashboardCard from '../../Components/DashboardCard/DashboardCard.js';
import './DashboardList.css';
import Form from 'react-bootstrap/Form';

class DashboardList extends React.Component {
  
  constructor () {
    super();
    this.searchInput = React.createRef();
  }
  state = {
    characters: [],
    initUri: 'https://rickandmortyapi.com/api/character/',
    count: 0,
    isLoading: true,
    finalUri:'',
    searchResult: [],
    searchText: '',
  }

  async componentDidMount() {
    await this.initialRequest();
    await this.loadCharacters();
  }

  async loadCharacters() {
    const uri = this.state.finalUri;
    const req = new Request(uri, {
      method: 'GET',
    });

    await fetch(req).then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { characters: data } );
    })
    .catch( err => alert('ERROR in loadCharacters:', err))
    this.setState( { isLoading: false } );
  }

  async initialRequest() {
    const uri = this.state.initUri;
    const req = new Request(uri, {
      method: 'GET',
    });

    await fetch(req).then( (response) => {
      return response.json();
    })
    .then( (data) => {
      const numberOfDudes = data.info.count;
      this.setState( { count: numberOfDudes } );
      let tmpUri = this.state.initUri;

      for(let i = 1; i <= numberOfDudes; i++) {
        if(i === 1) {
          tmpUri = tmpUri + i;
        } else {
          tmpUri = tmpUri + ',' + i;
        }
      }
      this.setState( { finalUri: tmpUri } );
    })
    .catch( err => alert('ERROR in initialRequest():', err))
  }

  searchForDude() {
    let searchRes = '';
    const dudeName = this.state.searchText.toLowerCase();

    searchRes = this.state.characters.filter( dude => {
      return dude.name.toLowerCase().includes(dudeName);
    });
    this.setState( {searchResult: searchRes} )
  }

  updateSearchText() {
    this.setState( { searchText:this.searchInput.current.value } )
    setTimeout(() => { this.searchForDude() }, 300 )
  }

  render() {

    const {searchResult, searchText, isLoading, characters} = this.state;
    let displayChars;
    
    if(searchText.length === 0) {
      displayChars = characters.slice(0, 20);
    } else {
      displayChars = searchResult;
    }

    return (
      <div className="">
        <Form>
          <Form.Group controlId="searchBar">
            <Form.Control 
              ref={this.searchInput}
              type="text"          
              onChange={() => this.updateSearchText()}     
              placeholder="Enter the name of a dude..."
            />
          </Form.Group>
        </Form>      
        <div className="row container-fluid justify-content-center" id="dashboard">
          {          
            !isLoading ? (
              displayChars.length ? (
                displayChars.map( (currChar) => {
                    return (
                      <DashboardCard key={currChar.id} currentCharacter={currChar}/>
                    );
                  }
                )
              ) : (
                <h3 className="place-holder">No matching result.</h3>
              )
            ) : (
              <h3 className="place-holder">Loading....</h3>
            )
          }
        </div>
      </div>
    )
  }

}

export default DashboardList;