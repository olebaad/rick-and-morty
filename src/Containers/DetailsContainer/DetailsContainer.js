import React from 'react';
import DetailsCard from '../../Components/DetailsCard/DetailsCard.js';

class DetailsContainer extends React.Component {
  
  state = {
    character: {},
    planet: {},
    uri: 'https://rickandmortyapi.com/api/character/',
    isLoading: true,
    planetUrl:'',
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    const characterUri = this.state.uri + id;
    
    await this.loadCharacter(characterUri);
    this.loadPlanet();
  }

  async loadCharacter(characterUri) {
    const req = new Request(characterUri, {
      method: 'GET',
    });

    await fetch(req).then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { character: data } );
    })
    .catch( err => alert('ERROR in loadCharacters:', err) )
    
    this.setState( { planetUrl: this.state.character.location.url } );
  }

  async loadPlanet() {
    const req = new Request(this.state.planetUrl, {
      method: 'GET',
    });

    await fetch(req).then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { planet: data } );
    })
    .catch( (err) => {
      this.setState( { planet: {
        name: 'unknown',
        type: 'unknown',
        dimension: 'unknown',
      }});
      alert(`ERROR in loadPlanet: \n\"${err}\" \n\nCommonly due to lack of location information about the character.`);
    })
    this.setState( { isLoading: false } );
  }

  render() {

    const {isLoading, character, planet} = this.state;
    return (
      <div className="">      
        {          
          !isLoading ? (
            <DetailsCard key={character.id} currentCharacter={character} currentPlanet={planet}/>            
          ) : (
            <h3>Loading....</h3>
          )
        }
      </div>
    )
  }

}

export default DetailsContainer;